
## 0.2.3 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/f5-vip-creation!11

---

## 0.2.2 [07-09-2021]

* Certify on IAP 2021.1

See merge request itentialopensource/pre-built-automations/f5-vip-creation!10

---

## 0.2.1 [02-23-2021]

* Update README.md, bundles/workflows/F5 VIP Deletion.json, bundles/workflows/F5...

See merge request itentialopensource/pre-built-automations/f5-vip-creation!8

---

## 0.2.0 [06-18-2020]

* Add './' to img path

See merge request itentialopensource/pre-built-automations/f5-vip-creation!5

---

## 0.1.0 [06-17-2020]

* [minor/LB-353] Rename artifact in manifest and package

See merge request itentialopensource/pre-built-automations/F5-VIP-Creation!4

---

## 0.0.2 [05-22-2020]

* added the additional information section

See merge request itentialopensource/pre-built-automations/F5_VIP_Creation!3

---\n
