<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your artifact name -->
# F5 VIP Creation

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)

## Overview

<!-- Write a few sentences about the artifact and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->
<!--
<table><tr><td>
  <img src="images/workflow.png" alt="workflow" width="800px">
</td></tr></table>
-->
<!-- REPLACE COMMENT ABOVE WITH IMAGE OF YOUR MAIN WORKFLOW -->

<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->
The VIP Creation workflow is an automation built to configure an F5 BigIP Load Balancer to create VIP Servers and apply appropriate profiles, irules and security settings during the process.  This is done via Ansible.
To learn more about the Ansible Bigip Virtual Server module click [HERE](https://docs.ansible.com/ansible/latest/modules/bigip_virtual_server_module.html)
<table><tr><td>
  <img src="./images/workflow.png" alt="workflow" width="800px">
</td></tr></table>

_Estimated Run Time_: 5 minutes

## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2021.1`
* Itential Automation Gateway

## Requirements

* F5 BigIP Load Balancer on-boarded on Ansible

<!-- Unordered list highlighting the requirements of the artifact -->
<!-- EXAMPLE -->
<!-- * cisco ios device -->
<!-- * Ansible or NSO (with F5 NED) * -->

## Features

* Allow Zero Touch operation (happy path)
* Notifications place holder


<!-- Unordered list highlighting the most exciting features of the artifact -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->


## Future Enhancements

<!-- OPTIONAL - Mention if the artifact will be enhanced with additional features on the road map -->
<!-- Ex.: This artifact would support Cisco XR and F5 devices -->
* Add NSO support

## How to Install

To install the artifact:

* Verify you are running a supported version of the Itential Automation Platform (IAP) and Itential Automation Gateway (IAG) as listed above in the [Requirements](#requirements) section in order to install the artifact.
* The artifact can be installed from within Admin Essentials. Simply search for the name of your desired artifact and click the install button (as shown below).

<!-- REPLACE BELOW WITH IMAGE OF YOUR PUBLISHED ARTIFACT -->
<table><tr><td>
  <img src="./images/install.png" alt="install" width="600px">
</td></tr></table>
<!-- REPLACE ABOVE WITH IMAGE OF YOUR PUBLISHED ARTIFACT -->

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this artifact can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use Automation Catalog to start this this use case, fill all required form fields, and navigate to the active job view to see its progress.

<!-- Explain the main entrypoint(s) for this artifact: Automation Catalog item, Workflow, Postman, etc. -->
## Additional Information

Please use your Itential Customer Success account if you need support when using this artifact.
